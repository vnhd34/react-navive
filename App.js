import React from 'react';

import { createStackNavigator } from 'react-navigation';

import LoginScreen from './components/auth/LoginScreen.js';
import SignUpScreen from './components/auth/SignUpScreen.js';
import PostScreen from './components/PostScreen.js';
import DetailPostScreen from './components/DetailPostScreen.js'


export default class App extends React.Component {

  render() {
    return <RootStack />
  }

}

const RootStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      header: { visible: false },
      navigationOptions: {
        title: 'Login',
        header: null
      },
    },

    Posts: {
      screen: PostScreen,
      navigationoptions: {
        title: 'Posts'
      }
    },

    SignUp: {
      screen: SignUpScreen,
      navigationoptions: {
        title: 'SignUp'
      }
    },

    DetailPost: {
      screen: DetailPostScreen
    }
  },

  {
    initialRouteName: 'Login',
  }
)
