import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default class LayoutItemPost extends React.Component {

	static navigationOptions = {
		title: 'Chi tiết bài viết',
	}

	render() {
		const item = this.props.navigation.getParam('listData');
		return (
			<ScrollView>
				<View style={styles.container}>
					<Text style={styles.textTitle}>
						{item.titles}
					</Text>
					<View style={{ width: '100%', alignItems: 'flex-start' }}>
						<Text style={{ fontWeight: '600', color: '#000', marginTop: 12, fontSize: 14, justifyContent: 'center' }}>
							Tin tức trong ngày
		               		</Text>
					</View>
					<View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', margin: 16 }}>
						<Image style={styles.thumbnail} source={{ uri: item.images }} />
					</View>
					<View style={styles.layoutContent}>
						<View style={{ flex: 1 }}>
							<Text style={styles.contents}>
								{item.contents}
							</Text>
						</View>
					</View>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({

	container: {
		alignItems: 'center',
		backgroundColor: '#fff',
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 20,
		paddingBottom: 24,
		borderBottomWidth: 1,
		borderBottomColor: 'lightgray',
	},

	thumbnail: {
		marginTop: 10,
		width: '100%',
		height: 250
	},

	content: {
		fontSize: 15,
		color: '#555',
	},

	layoutContent: {
		marginTop: 10,
		flexDirection: 'row',
	},

	textTitle: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#333'
	}

});