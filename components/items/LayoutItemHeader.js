import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

export default class LayoutItemHeader extends React.Component {

	render() {
		return (
			<TouchableOpacity style={styles.container} onPress={this.props.onPress}  onLongPress={this.props.onLongPress}>
				<View style={{ borderBottomWidth: 3, borderBottomColor: '#000', paddingBottom: 24, width: '100%' }}>
					<Image style={styles.thumbnail} source={{uri: this.props.data.images}} />
					<Text style={styles.textTitle}>
						{this.props.data.titles}
					</Text>
					<Text style={{ fontWeight: '600', color: '#FF0000', marginTop: 8, fontSize: 14 }}>
								Tin nóng
	               				 </Text>
					<View style={styles.layoutContent}>
						<Text style={styles.content}>
							{this.props.data.news}
						</Text>
					</View>
				</View>
			</TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({

	container: {
		alignItems: 'center',
		backgroundColor: '#fff',
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 20,

	},

	thumbnail: {
		width: '100%',
		height: 250
	},

	content: {
		fontSize: 14,
		color: '#555',
	},

	layoutContent: {
		marginTop: 10,
		flexDirection: 'row',
		flex: 1
	},

	textTitle: {
		marginTop: 5,
		fontSize: 20,
		fontWeight: 'bold',
		color: '#333'
	}

});