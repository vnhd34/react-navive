import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

export default class LayoutItemPost extends React.Component {

	render() {
		return (
			<TouchableOpacity style={styles.container} onPress={this.props.onPress} onLongPress={this.props.onLongPress}>
				<View>
					<Text style={styles.textTitle}>
						{this.props.data.titles}
					</Text>
					<View style={styles.layoutContent}>
						<View style={{ flex: 1, marginEnd: 16 }}>
							<Text style={styles.content}>
								{this.props.data.news}
							</Text>
							<Text style={{ fontWeight: '600', color: '#FF0000', marginTop: 8, fontSize: 14 }}>
								Tin tức
	               				 </Text>
						</View>
						<Image style={styles.thumbnail} source={{uri: this.props.data.images}} />
					</View>
				</View>
			</TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({

	container: {
		alignItems: 'center',
		backgroundColor: '#fff',
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 20,
		paddingBottom: 24,
		borderBottomWidth: 0.5,
		borderBottomColor: 'lightgray'
	},

	thumbnail: {
		width: 200,
		height: 125
	},

	content: {
		fontSize: 14,
		color: '#555',
	},

	layoutContent: {
		marginTop: 10,
		flexDirection: 'row',
	},

	textTitle: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#333'
	}

});