import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Modal,TextInput } from 'react-native';

export default class AddPost extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        }
        this.openModal = this.openModal.bind(this);
    }

    openModal() {
        this.setState({ modalVisible: true });
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }


    render() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }}>

                    <View style={{ flex: 3, flexDirection: 'column' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, textAlign: 'center', marginTop: 20 }} >New Post</Text>
                        <TextInput
                            style={styles.styleTextInput}
                            placeholder="Type here to Title!"
                            onChangeText={(title) => this.setState({ title })}
                        />
                        <TextInput
                            style={styles.styleTextInput}
                            placeholder="Type here to News!"
                            onChangeText={(news) => this.setState({ news })}
                        />
                        <TextInput
                            style={styles.styleTextInput}
                            placeholder="Type here to Images!"
                            onChangeText={(image) => this.setState({ image })}
                        />
                        <TextInput
                            style={{ height: 100, margin: 10, padding: 10, textAlignVertical: 'top' }}
                            multiline={true}
                            placeholder="Type here to Content!"
                            onChangeText={(content) => this.setState({ content })}
                        />

                        <View style={{ justifyContent: 'center' }}>
                            <TouchableOpacity style={styles.buttonStyle}>
                                <Text style={{ color: '#FFFF' }}>Post</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    flatlist: {
        width: '100%'
    },
    buttonStyle: {
        backgroundColor: '#FF0000',
        margin: 10,
        padding: 15,
        borderRadius: 5,
        alignItems: 'center'
    },
    modalContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    styleTextInput: {
        height: 40,
        margin: 10,
        padding: 10
    }
})
