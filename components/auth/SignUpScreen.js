import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { firebaseApp } from '../firebase/FireBaseConfig'

export default class SignUp extends React.Component {

  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
    this.SignUp = this.SignUp.bind(this);
  }

  SignUp() {
    if(this.state.email.length == 0 || this.state.password.length == 0){
      alert('No empty email or password');
      return
    }
    firebaseApp.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        Alert.alert(
          'Signup susscess'
        )
        setTimeout(() => {
          this.props.navigation.navigate('Login');
        }, 2000)
      })
      .catch(function (error) {
        Alert.alert(error)
      });
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>

        <View>
          {/*Title App*/}
          <View style={{ marginBottom: 30, marginTop: 30 }}>
            <Text style={styles.textTitleApp}>Register</Text>
          </View>

          {/*Layout Usernam & Password*/}
        </View>

        <View style={{ flex: 3, flexDirection: 'column', marginTop: 50 }}>
          <View style={styles.viewInputStyle}>
            <Image source={require('../../images/email.png')} style={styles.imageView} />
            <TextInput
              style={{ flex: 1 }}
              onChangeText={(email) => this.setState({email})}
              placeholder='Enter Your Email Here'
              underlineColorAndroid='transparent'
              keyboardType='email-address' />
          </View>

          <View style={styles.viewInputStyle}>
            <Image source={require('../../images/lock.png')} style={styles.imageView} />
            <TextInput
              style={{ flex: 1 }}
              onChangeText={(password) => this.setState({ password })}
              secureTextEntry
              placeholder='Enter Your Password Here'
              underlineColorAndroid='transparent' />
          </View>
          {/* SignUp */}
          <View style={{ justifyContent: 'center' }}>
            <TouchableOpacity style={styles.buttonStyle}
              onPress={this.SignUp}
            >
              <Text style={{ color: '#FFF' }}>SignUp</Text>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    );
  }
}



const styles = StyleSheet.create({

  textTitleApp: {
    fontSize: 45,
    fontWeight: '600',
    textShadowColor: '#3333',
    textAlign: 'center',
    color: '#000',
    marginTop: 80,
    elevation: 20
  },



  textDesApp: {
    fontSize: 20,
    fontWeight: '200',
    textShadowColor: '#3333',
    textAlign: 'center',
    color: '#ffff',
    marginTop: 5,
    elevation: 20
  },

  iconLogin: {
    marginLeft: 10,
    padding: 10,
    justifyContent: 'center'
  },



  textBottomLogin: {
    fontSize: 14,
    marginTop: 20,
    textShadowColor: '#3333',
    textAlign: 'center',
    color: '#ffff',
    marginTop: 5,
    elevation: 20
  },



  textInputStyle: {
    height: 50,
    color: '#fff',
    fontSize: 18,
    marginLeft: 5,
    borderRadius: 50,
    flex: 1
  },


  keyboardAvo: {
    width: '100%',
    justifyContent: 'center',
  },

  viewChild: {
    flex: 3,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 30
  },
  iconLogo: {
    width: 72,
    borderRadius: 5,
    height: 72,
    marginTop: 30
  },
  textLogo: {
    width: '100%',
    height: 70,
    textAlign: "center",
    fontSize: 20,
    color: '#333333',
    marginTop: 10,
    fontWeight: 'bold'
  },
  imageView: {
    height: 25,
    width: 25,
    borderColor: "#CCCCCC",
    margin: 10,
    padding: 10,
    alignItems: 'center'
  },
  viewInputStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#DDDDDD',
    height: 50,
    borderRadius: 5,
    margin: 30,
    marginTop: -20
  },

  buttonStyle: {
    backgroundColor: '#FF0000',
    margin: 30,
    padding: 15,
    borderRadius: 5,
    marginTop: -10,
    alignItems: 'center'
  }
});
