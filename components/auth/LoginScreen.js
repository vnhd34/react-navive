import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { firebaseApp } from '../firebase/FireBaseConfig.js'

console.ignoredYellowBox = [
  'Setting a timer'
];

export default class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
    this.checkLogin = this.checkLogin.bind(this);
  }

  checkLogin() {
    if(this.state.email.length == 0 || this.state.password.length == 0){
      alert('No empty email or password');
      return
    }
    firebaseApp.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        alert('Login success');
        setTimeout(() => {
          this.props.navigation.navigate('Posts');
        }, 2000)
      })
      .catch(function (error) {
        Alert.alert(error)
      });
  }
  render() {
    return (

      <View style={styles.container}>

        <View style={styles.viewChild}>
          <View style={styles.iconLogo} />
          <Text style={styles.textLogo}>WELLCOME</Text>
        </View>

        {/* Edittext */}
        <View style={{ flex: 3, flexDirection: 'column' }}>

          <View style={styles.viewInputStyle}>
            <Image source={require('..//../images/email.png')} style={styles.imageView} />
            <TextInput
              style={{ flex: 1 }}
              onChangeText={(email) => this.setState({ email })}
              placeholder='Enter Your Email Here'
              underlineColorAndroid='transparent'
              keyboardType='email-address' />
          </View>


          <View style={styles.viewInputStyle}>
            <Image source={require('..//../images/lock.png')} style={styles.imageView} />
            <TextInput
              style={{ flex: 1 }}
              onChangeText={(password) => this.setState({ password })}
              secureTextEntry={true}
              placeholder='Enter Your Password Here'
              underlineColorAndroid='transparent' />
          </View>

          <View style={{ justifyContent: 'center' }}>
            <TouchableOpacity style={styles.buttonStyle}
              onPress={this.checkLogin} >
              {/* onPress={this.props.navigation.navigate('Posts')} > */}

              <Text style={{ color: '#FFFF' }}>Login</Text>
            </TouchableOpacity>
          </View>

        </View>

        {/* infor */}
        <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'center' }}>
          <Text>Don't have an acount? </Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
            <Text style={{ color: '#FF0000' }}>Sign Up</Text>
          </TouchableOpacity>

        </View>


      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  viewChild: {
    flex: 3,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 30
  },
  iconLogo: {
    width: 72,
    borderRadius: 5,
    height: 72,
    marginTop: 30,
    backgroundColor: '#FF0000'
  },
  textLogo: {
    width: '100%',
    height: 70,
    textAlign: "center",
    fontSize: 20,
    color: '#333333',
    marginTop: 10,
    fontWeight: 'bold'
  },
  imageView: {
    height: 25,
    width: 25,
    borderColor: "#CCCCCC",
    margin: 10,
    padding: 10,
    alignItems: 'center'
  },
  viewInputStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#DDDDDD',
    height: 50,
    borderRadius: 5,
    margin: 30,
    marginTop: -20
  },

  buttonStyle: {
    backgroundColor: '#FF0000',
    margin: 30,
    padding: 15,
    borderRadius: 5,
    marginTop: -10,
    alignItems: 'center'
  }
});
