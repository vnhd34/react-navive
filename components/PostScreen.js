import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Modal, TextInput, FlatList, ActivityIndicator } from 'react-native';
import LayoutItemPost from './items/LayoutItemPost.js';
import LayoutItemHeader from './items/LayoutItemHeader.js';
import { firebaseApp } from './firebase/FireBaseConfig.js'
import ActionButton from 'react-native-action-button';

export default class Post extends React.Component {

	static navigationOptions = {
		title: 'Trang chủ',
	}

	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false,
			dialogVisible: false,
			isLoading: false,
			titles: '',
			contents: '',
			images: '',
			news: '',
			category: '',
			listData: [],
			indexList: 0,
		}
		this.openModal = this.openModal.bind(this);
		this.itemRef = firebaseApp.database();
	}

	openModal() {
		this.setState({ modalVisible: true });
	}

	setModalVisible(visible) {
		this.setState({ modalVisible: visible });
	}

	openDialog(index) {
		this.setState({ titles: this.state.listData[index].titles })
		this.setState({ contents: this.state.listData[index].contents })
		this.setState({ images: this.state.listData[index].images })
		this.setState({ news: this.state.listData[index].news })
		this.setState({ dialogVisible: true });
		if (this.state.listData.length == 1) {
			this.setState({
				indexList: 0
			})
		} else {
			this.setState({
				indexList: index
			})
		}

	}

	setDialogVisible(visible) {
		this.setState({ dialogVisible: visible });
	}

	onclickItem(index) {
		this.props.navigation.navigate('DetailPost', { listData: this.state.listData[index] })

	}

	pushPost() {
		this.itemRef.ref('post').push({
			titles: this.state.titles,
			news: this.state.news,
			images: this.state.images,
			contents: this.state.contents,
			category: 'News',
		})
		this.listenForItems(this.itemRef);
		alert('Post success');
		this.setModalVisible(!this.state.modalVisible);
	}

	editPost(item) {
		this.itemRef.ref('post/' + item.key).set({
			titles: this.state.titles,
			news: this.state.news,
			images: this.state.images,
			contents: this.state.contents,
			category: 'News',
		}, function (error) {
			if (error) {
				alert(error);
			}
			return
		});
		this.listenForItems(this.itemRef);
		alert('Edit success');
		this.setDialogVisible(!this.state.dialogVisible);
	}

	deletePost(item) {
		this.itemRef.ref('post').child(item.key).remove();
		this.listenForItems(this.itemRef);
		alert('Delete success');
		this.setDialogVisible(!this.state.dialogVisible);
	}

	listenForItems(itemRef) {
		let list = []
		itemRef.ref('post').on('value', (snapshot) => {
			snapshot.forEach((child) => {
				var item = child.val();
				item.key = child.key;
				list.push(item)
			});
			this.setState({
				listData: list
			});
			this.setState({
				isLoading: true
			});
		});
	}

	render() {

		if (!this.state.isLoading) {
			return (
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
					<ActivityIndicator size="small" color="#FF0000" />
					<Text style={{ marginTop: 10 }}>Đang tải dữ liệu</Text>
				</View>
			);
		} else {
			return (
				<View>
					<FlatList
						data={this.state.listData}
						renderItem={({ item, index }) => index === 0 ?
							<LayoutItemHeader data={item} onPress={() => { this.onclickItem(index) }} onLongPress={() => { this.openDialog(index) }} /> :
							<LayoutItemPost data={item} onPress={() => { this.onclickItem(index) }} onLongPress={() => { this.openDialog(index) }} />}
						contentContainerStyle={styles.flatlist}
					/>
					<ActionButton
						buttonColor="rgba(231,76,60,1)"
						onPress={this.openModal} />

					<Modal
						animationType="slide"
						transparent={false}
						visible={this.state.modalVisible}
						onRequestClose={() => {
							this.setModalVisible(!this.state.modalVisible);
						}}>

						<View style={{ flex: 3, flexDirection: 'column' }}>
							<Text style={{ fontWeight: 'bold', fontSize: 20, textAlign: 'center', marginTop: 20 }} >New Post</Text>
							<TextInput
								style={styles.styleTextInput}
								placeholder="Type here to Title!"
								onChangeText={(titles) => this.setState({ titles })}
							/>
							<TextInput
								style={styles.styleTextInput}
								placeholder="Type here to News!"
								onChangeText={(news) => this.setState({ news })}
							/>
							<TextInput
								style={styles.styleTextInput}
								placeholder="Type here to Images!"
								onChangeText={(images) => this.setState({ images })}
							/>
							<TextInput
								style={{ height: 100, margin: 10, padding: 10, textAlignVertical: 'top' }}
								multiline={true}
								placeholder="Type here to Content!"
								onChangeText={(contents) => this.setState({ contents })}
							/>

							<View style={{ justifyContent: 'center' }}>
								<TouchableOpacity style={styles.buttonStyle}
									onPress={() => { this.pushPost() }}>
									<Text style={{ color: '#FFFF' }}>Post</Text>
								</TouchableOpacity>
							</View>
						</View>
					</Modal>

					<Modal
						animationType="slide"
						transparent={false}
						visible={this.state.dialogVisible}
						onRequestClose={() => {
							this.setDialogVisible(!this.state.dialogVisible);
						}}>

						<View style={{ flex: 3, flexDirection: 'column' }}>

							<Text style={{ fontWeight: 'bold', fontSize: 20, textAlign: 'center', marginTop: 20 }} >Edit Post</Text>
							<TextInput
								style={styles.styleTextInput}
								value={this.state.titles}
								onChangeText={(titles) => this.setState({ titles })}
							/>
							<TextInput
								style={styles.styleTextInput}
								value={this.state.news}
								onChangeText={(news) => this.setState({ news })}
							/>
							<TextInput
								style={styles.styleTextInput}
								value={this.state.images}
								onChangeText={(images) => this.setState({ images })}
							/>
							<TextInput
								style={{ height: 100, margin: 10, padding: 10, textAlignVertical: 'top' }}
								multiline={true}
								value={this.state.contents}
								onChangeText={(contents) => this.setState({ contents })}
							/>

							<View style={{ justifyContent: 'center' }}>
								<TouchableOpacity style={styles.buttonStyle}
									onPress={() => { this.editPost(this.state.listData[this.state.indexList]) }}>
									<Text style={{ color: '#FFFF' }}>Update</Text>
								</TouchableOpacity>
							</View>

							<View style={{ justifyContent: 'center' }}>
								<TouchableOpacity style={styles.buttonStyle}
									onPress={() => { this.deletePost(this.state.listData[this.state.indexList]) }}>
									<Text style={{ color: '#FFFF' }}>Delete</Text>
								</TouchableOpacity>
							</View>

						</View>
					</Modal>
				</View>
			);
		}
	}

	componentDidMount() {
		this.listenForItems(this.itemRef);
	};
}

const styles = StyleSheet.create({
	flatlist: {
		width: '100%'
	},

	buttonStyle: {
		backgroundColor: '#FF0000',
		margin: 10,
		padding: 15,
		borderRadius: 5,
		alignItems: 'center'
	},

	modalContent: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		paddingTop: 20
	},

	styleTextInput: {
		height: 40,
		margin: 10,
		padding: 10
	}
})
